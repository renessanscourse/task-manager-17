package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findUserTasks(String userId);

    public List<Task> findAllTasks();

    void removeTask(String userId, Task task);

    void removeAllTasks(String userId);

    Task findTaskById(String userId, String id);

    Task findTaskByIndex(String userId, Integer index);

    Task findTaskByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task removeTaskById(String userId, String id);

    Task removeTaskByIndex(String userId, Integer index);

    Task removeTaskByName(String userId, String name);

    void mergeArray(Task... tasks);

    Task mergeOne(Task task);

    void mergeCollection(Collection<Task> tasks);

    void load(Collection<Task> tasks);

    void load(Task... tasks);

    void clear();

}