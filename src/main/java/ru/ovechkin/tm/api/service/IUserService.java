package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String userId, String login);

    User lockUserByLogin(String userId, String login);

    User unLockUserByLogin(String login);

    User mergeOne(User user);

    void merge(User... users);

    void merge(Collection<User> tasks);

    void load(Collection<User> tasks);

    void load(User... tasks);

    void clear();

}