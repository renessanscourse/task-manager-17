package ru.ovechkin.tm.command.project;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_CLEAR;
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectService().removeAllProjects(userId);
        System.out.println("[OK]");
    }

}