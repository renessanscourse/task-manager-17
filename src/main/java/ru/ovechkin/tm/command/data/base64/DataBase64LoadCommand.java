package ru.ovechkin.tm.command.data.base64;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.DATA_BASE64_LOAD;
    }

    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BYTE64 LOAD]");
        final String base64date = new String(Files.readAllBytes(Paths.get("./data.base64")));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        final ObjectInputStream objectInputStream  = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

}