package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.ICommandRepository;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.command.auth.*;
import ru.ovechkin.tm.command.data.base64.DataBase64LoadCommand;
import ru.ovechkin.tm.command.data.base64.DataBase64SaveCommand;
import ru.ovechkin.tm.command.data.binary.DataBinaryLoadCommand;
import ru.ovechkin.tm.command.data.binary.DataBinarySaveCommand;
import ru.ovechkin.tm.command.project.*;
import ru.ovechkin.tm.command.system.*;
import ru.ovechkin.tm.command.task.*;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            HelpCommand.class, AboutCommand.class, VersionCommand.class, CommandsAllCommand.class,
            ArgumentsAllCommand.class, SystemInfoCommand.class,

            TaskCreateCommand.class, TaskClearCommand.class,
            TaskListCommand.class, TaskShowByIdCommand.class, TaskShowByIndexCommand.class,
            TaskShowByNameCommand.class, TaskUpdateByIdCommand.class, TaskUpdateByIndexCommand.class,
            TaskRemoveByIdCommand.class, TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class,

            ProjectCreateCommand.class, ProjectClearCommand.class, ProjectListCommand.class,
            ProjectShowByIdCommand.class, ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class,
            ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectRemoveByIdCommand.class,
            ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class,

            UserShowProfileCommand.class, UserRegistryCommand.class, LoginCommand.class, LogoutCommand.class,
            UserUpdatePasswordCommand.class, UserUpdateProfileCommand.class,

            UserLockCommand.class, UserUnLockCommand.class, UserRemoveCommand.class,

            DataBinaryLoadCommand.class, DataBinarySaveCommand.class, DataBase64SaveCommand.class,
            DataBase64LoadCommand.class,

            ExitCommand.class
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}