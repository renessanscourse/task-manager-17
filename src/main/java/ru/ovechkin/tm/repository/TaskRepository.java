package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.IndexUnknownException;
import ru.ovechkin.tm.exeption.unknown.NameUnknownException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, Task task) {
        final List<Task> result = new ArrayList<>();
        for (final Task iterator : tasks) {
            if (userId.equals(iterator.getUserId())) result.add(iterator);
        }
        result.remove(task);
    }

    @Override
    public List<Task> findAllUserTask(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public List<Task> findAllTasks() {
        return tasks;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> tasks = findAllUserTask(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public Task findById(final String userId, final String id) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                if (id.equals(task.getId())) return task;
            }
        }
        throw new IdUnknownException();
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                if (tasks.indexOf(task) == index) return task;
            }
        }
        throw new IndexUnknownException(index);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                if (name.equals(task.getName())) return task;
            }
        }
        throw new NameUnknownException();
    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) throw new IdUnknownException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task merge(final Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void merge(final Task... tasks) {
        for (final Task task : tasks) merge(task);
    }

    @Override
    public void merge(final Collection<Task> tasks) {
        for (final Task task : tasks) merge(task);
    }

    @Override
    public void load(final Collection<Task> tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(final Task... tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}