package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.other.WrongCurrentPasswordException;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.exeption.user.AlreadyExistLoginException;
import ru.ovechkin.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new LoginUnknownException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        if (userId == null) throw new NotLoggedInException();
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        if (userService.findByLogin(login) != null) throw new AlreadyExistLoginException(login);
        userService.create(login, password, email);
    }

    public User findUserByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return userService.findById(userId);
    }

    @Override
    public void updateProfileInfo(
            final String newLogin,
            final String newFirstName,
            final String newMiddleName,
            final String newLastName,
            final String newEmail
    ) {
        final String userId = getUserId();
        final User user = findUserByUserId(userId);
        if (user == null) throw new UserEmptyException();
        if (newLogin == null || newLogin.isEmpty()) throw new LoginEmptyException();
        user.setLogin(newLogin);
        if (newFirstName == null || newFirstName.isEmpty()) throw new FirstNameEmptyException();
        user.setFirstName(newFirstName);
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new MiddleNameEmptyException();
        user.setMiddleName(newMiddleName);
        if (newLastName == null || newLastName.isEmpty()) throw new LastNameEmptyException();
        user.setLastName(newLastName);
        if (newEmail == null || newEmail.isEmpty()) throw new EmailEmptyException();
        user.setEmail(newEmail);
    }

    @Override
    public void updatePassword(final String currentPassword, final String newPassword) {
        if (currentPassword == null || currentPassword.isEmpty()) throw new PasswordEmptyException();
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordEmptyException();
        final String userId = getUserId();
        final User user = findUserByUserId(userId);
        final String currentPasswordCheckHash = HashUtil.salt(currentPassword);
        final String currentUserPasswordHash = user.getPasswordHash();
        if (!currentPasswordCheckHash.equals(currentUserPasswordHash)) {
            throw new WrongCurrentPasswordException();
        }
        final String newPasswordHash = HashUtil.salt(newPassword);
        user.setPasswordHash(newPasswordHash);
    }

}