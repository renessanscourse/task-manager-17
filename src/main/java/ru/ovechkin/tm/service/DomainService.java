package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    public DomainService(
            final ITaskService taskService,
            final IProjectService projectService,
            final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAllProjects());
        domain.setTasks(taskService.findAllTasks());
        domain.setUsers(userService.findAll());
    }

}
